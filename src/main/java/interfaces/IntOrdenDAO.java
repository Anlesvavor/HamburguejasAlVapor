package interfaces;
;
import modelos.Orden;

import java.util.List;

public interface IntOrdenDAO {
    public void create(Orden obj);

    public Orden read(Integer id);

    public List<Orden> read(String criterio);

    public void update(Orden obj);

    public void delete(Integer id);
}
