package interfaces;

import modelos.Proveedor;

import java.util.List;

public interface IntProveedorDAO {
    public void create(Proveedor obj);

    public Proveedor read(Integer id);

    public List<Proveedor> read(String criterio);

    public void update(Proveedor obj);

    public void delete(Integer id);
}
