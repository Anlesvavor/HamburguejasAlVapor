package interfaces;

import modelos.Platillo;

import java.util.List;

public interface IntPlatilloDAO {
    public void create(Platillo obj);

    public Platillo read(Integer id);

    public List<Platillo> read(String criterio);

    public void update(Platillo obj);

    public void delete(Integer id);

}
