package interfaces;

import modelos.Inventario;

import java.util.List;

public interface IntInventarioDAO {
    public void create(Inventario obj);

    public Inventario read(Integer id);

    public List<Inventario> read(String criterio);

    public void update(Inventario obj);

    public void delete(Integer id);
}
