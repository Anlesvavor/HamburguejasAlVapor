package interfaces;

import modelos.Cliente;

import java.util.List;

public interface IntClienteDAO {
    public void create(Cliente obj);

    public Cliente read(Integer id);

    public List<Cliente> read(String criterio);

    public void update(Cliente obj);

    public void delete(Integer id);
}
