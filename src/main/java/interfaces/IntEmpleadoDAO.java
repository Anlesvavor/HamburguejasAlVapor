package interfaces;

import modelos.Empleado;

import java.util.List;

public interface IntEmpleadoDAO {

    public void create(Empleado obj);

    public Empleado read(Integer id);

    public List<Empleado> read(String criterio);

    public void update(Empleado obj);

    public void delete(Integer id);

}
