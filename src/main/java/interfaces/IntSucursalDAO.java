package interfaces;

import modelos.Sucursal;

import java.util.List;

public interface IntSucursalDAO {
    public void create(Sucursal obj);

    public Sucursal read(Integer id);

    public List<Sucursal> read(String criterio);

    public void update(Sucursal obj);

    public void delete(Integer id);
}
