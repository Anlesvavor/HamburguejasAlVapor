package implementaciones;

import conexiones.Conexion;
import interfaces.IntInventarioDAO;
import modelos.Empleado;
import modelos.Inventario;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class InventarioDAO implements IntInventarioDAO {

    @Override
    public void create(Inventario obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Inventario.INSERT);
            Integer i = 1;
            ps.setInt(i++, obj.getIdProducto());
            ps.setString(i++, obj.getProducto());
            ps.setString(i++, obj.getDescripcion());
            ps.setInt(i++, obj.getCantidad());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Inventario read(Integer id) {
        Inventario inventario = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Inventario.Q_BY_ID, id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                inventario = makeInventario(rs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return inventario;
    }

    @Override
    public List<Inventario> read(String criterio) {
        List<Inventario> inventarios = null;
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConnection().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Inventario.Q_ALL, criterio));
            if (rs.next()){
                inventarios.add(makeInventario(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return inventarios;
    }

    @Override
    public void update(Inventario obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Inventario.UPDATE, obj.getIdProducto()));
            Integer i = 1;
            ps.setInt(i++, obj.getIdProducto());
            ps.setString(i++, obj.getProducto());
            ps.setString(i++, obj.getDescripcion());
            ps.setInt(i++, obj.getCantidad());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Inventario.DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Inventario makeInventario(ResultSet rs){
        Integer i = 1;
        Inventario inventario = null;
        try {
            inventario = new Inventario(
                    rs.getInt(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getInt(i++)
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return inventario;
    }
}
