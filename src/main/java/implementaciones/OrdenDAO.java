package implementaciones;

import conexiones.Conexion;
import interfaces.IntClienteDAO;
import interfaces.IntOrdenDAO;
import interfaces.IntPlatilloDAO;
import modelos.Orden;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class OrdenDAO implements IntOrdenDAO {
    private IntClienteDAO clienteDAO = new ClienteDAO();
    private IntPlatilloDAO platilloDAO = new PlatilloDAO();


    @Override
    public void create(Orden obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Orden.INSERT);
            Integer i = 1;
            ps.setInt(i++, obj.getIdOrden());
            ps.setInt(i++, obj.getCliente().getIdCliente());
            ps.setInt(i++, obj.getPlatillo().getIdPlatillo());
            ps.setInt(i++, obj.getIdOrden());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Orden read(Integer id) {
        Orden orden = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Orden.Q_BY_ID, id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                orden = makeOrden(rs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return orden;
    }

    @Override
    public List<Orden> read(String criterio) {
        List<Orden> ordens = null;
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConnection().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Orden.Q_ALL, criterio));
            if (rs.next()){
                ordens.add(makeOrden(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return ordens;
    }

    @Override
    public void update(Orden obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Orden.UPDATE, obj.getIdOrden()));
            Integer i = 1;
            ps.setInt(i++, obj.getIdOrden());
            ps.setInt(i++, obj.getCliente().getIdCliente());
            ps.setInt(i++, obj.getPlatillo().getIdPlatillo());
            ps.setInt(i++, obj.getIdOrden());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Orden.DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Orden makeOrden(ResultSet rs){
        Integer i = 1;
        Orden orden = null;
        try {
            orden = new Orden(
                    rs.getInt(i++),
                    clienteDAO.read(rs.getInt(i++)),
                    platilloDAO.read(rs.getInt(i++)),
                    rs.getInt(i++)
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orden;
    }
}
