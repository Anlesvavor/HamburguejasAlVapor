package implementaciones;

import conexiones.Conexion;
import interfaces.IntEmpleadoDAO;
import modelos.Empleado;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class EmpleadoDAO implements IntEmpleadoDAO {
    @Override
    public void create(Empleado obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Empleado.INSERT);
            Integer i = 1;
            ps.setInt(i++, obj.getIdEmpleado());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getTelefono());
            ps.setString(i++, obj.getPuesto());
            ps.setInt(i++, obj.getSueldoBase());
            ps.setFloat(i++, obj.getBonos());
            ps.setInt(i++, obj.getPermiso());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Empleado read(Integer id) {
        Empleado empleado = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Empleado.Q_BY_ID, id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                empleado = makeEmpleado(rs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return empleado;
    }

    @Override
    public List<Empleado> read(String criterio) {
        List<Empleado> empleados = null;
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConnection().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Empleado.Q_ALL, criterio));
            if (rs.next()){
                empleados.add(makeEmpleado(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return empleados;
    }

    @Override
    public void update(Empleado obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Empleado.UPDATE, obj.getIdEmpleado()));
            Integer i = 1;
            ps.setInt(i++, obj.getIdEmpleado());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getTelefono());
            ps.setString(i++, obj.getPuesto());
            ps.setInt(i++, obj.getSueldoBase());
            ps.setFloat(i++, obj.getBonos());
            ps.setInt(i++, obj.getPermiso());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Empleado.DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Empleado makeEmpleado(ResultSet rs){
        Integer i = 1;
        Empleado empleado = null;
        try {
            empleado = new Empleado(
                    rs.getInt(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getInt(i++),
                    rs.getFloat(i++),
                    rs.getInt(i++)
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return empleado;
    }
}
