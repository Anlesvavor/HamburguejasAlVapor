package implementaciones;

import conexiones.Conexion;
import interfaces.IntSucursalDAO;
import modelos.Empleado;
import modelos.Sucursal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SucursalDAO implements IntSucursalDAO {

    private EmpleadoDAO empleadoDAO = new EmpleadoDAO();

    @Override
    public void create(Sucursal obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Sucursal.INSERT);
            Integer i = 1;
            ps.setInt(i++, obj.getIdSucursal());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getDireccion());
            ps.setString(i++, obj.getTelefono());
            ps.setInt(i++, obj.getGenrente().getIdEmpleado());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Sucursal read(Integer id) {
        Sucursal sucursal = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Sucursal.Q_BY_ID, id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                sucursal = makeSucursal(rs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return sucursal;
    }

    @Override
    public List<Sucursal> read(String criterio) {
        List<Sucursal> sucursals = null;
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConnection().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Sucursal.Q_ALL, criterio));
            if (rs.next()){
                sucursals.add(makeSucursal(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return sucursals;
    }

    @Override
    public void update(Sucursal obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Sucursal.UPDATE, obj.getIdSucursal()));
            Integer i = 1;
            ps.setInt(i++, obj.getIdSucursal());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getDireccion());
            ps.setString(i++, obj.getTelefono());
            ps.setInt(i++, obj.getGenrente().getIdEmpleado());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Sucursal.DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Sucursal makeSucursal(ResultSet rs){
        Integer i = 1;
        Sucursal sucursal = null;
        try {
            sucursal = new Sucursal(
                    rs.getInt(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    empleadoDAO.read(rs.getInt(i++))
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sucursal;
    }
}
