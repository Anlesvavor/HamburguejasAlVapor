package implementaciones;

import conexiones.Conexion;
import interfaces.IntProveedorDAO;
import modelos.Proveedor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ProveedorDAO implements IntProveedorDAO {

    @Override
    public void create(Proveedor obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Proveedor.INSERT);
            Integer i = 1;
            ps.setInt(i++, obj.getIdProveedor());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getProducto());
            ps.setInt(i++, obj.getCantidad());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Proveedor read(Integer id) {
        Proveedor proveedor = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Proveedor.Q_BY_ID, id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                proveedor = makeProveedor(rs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return proveedor;
    }

    @Override
    public List<Proveedor> read(String criterio) {
        List<Proveedor> proveedors = null;
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConnection().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Proveedor.Q_ALL, criterio));
            if (rs.next()){
                proveedors.add(makeProveedor(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return proveedors;
    }

    @Override
    public void update(Proveedor obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Proveedor.UPDATE, obj.getIdProveedor()));
            Integer i = 1;
            ps.setInt(i++, obj.getIdProveedor());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getProducto());
            ps.setInt(i++, obj.getCantidad());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Proveedor.DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Proveedor makeProveedor(ResultSet rs){
        Integer i = 1;
        Proveedor proveedor = null;
        try {
            proveedor = new Proveedor(
                    rs.getInt(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getInt(i++)
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return proveedor;
    }
}
