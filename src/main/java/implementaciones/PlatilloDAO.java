package implementaciones;

import conexiones.Conexion;
import interfaces.IntPlatilloDAO;
import modelos.Platillo;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class PlatilloDAO implements IntPlatilloDAO {
    @Override
    public void create(Platillo obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Platillo.INSERT);
            Integer i = 1;
            ps.setInt(i++, obj.getIdPlatillo());
            ps.setString(i++, obj.getTipo());
            ps.setString(i++, obj.getNomPlato());
            ps.setString(i++, obj.getDescripcion());
            ps.setFloat(i++, obj.getPrecio());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Platillo read(Integer id) {
        Platillo platillo = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Platillo.Q_BY_ID, id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                platillo = makePlatillo(rs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return platillo;
    }

    @Override
    public List<Platillo> read(String criterio) {
        List<Platillo> platillos = null;
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConnection().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Platillo.Q_ALL, criterio));
            if (rs.next()){
                platillos.add(makePlatillo(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return platillos;
    }

    @Override
    public void update(Platillo obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Platillo.UPDATE, obj.getIdPlatillo()));
            Integer i = 1;
            ps.setInt(i++, obj.getIdPlatillo());
            ps.setString(i++, obj.getTipo());
            ps.setString(i++, obj.getNomPlato());
            ps.setString(i++, obj.getDescripcion());
            ps.setFloat(i++, obj.getPrecio());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Platillo.DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Platillo makePlatillo(ResultSet rs){
        Integer i = 1;
        Platillo platillo = null;
        try {
            platillo = new Platillo(
                    rs.getInt(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getFloat(i++)
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return platillo;
    }
}
