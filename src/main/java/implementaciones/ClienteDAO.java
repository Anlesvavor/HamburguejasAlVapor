package implementaciones;

import conexiones.Conexion;
import interfaces.IntClienteDAO;
import interfaces.IntGetLastIndex;
import modelos.Cliente;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class ClienteDAO implements IntClienteDAO, IntGetLastIndex {
    @Override
    public void create(Cliente obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Cliente.INSERT);
            Integer i = 1;
            ps.setInt(i++, obj.getIdCliente());
            ps.setString(i++, obj.getUsuario());
            ps.setString(i++, obj.getContrasena());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getaPaterno());
            ps.setString(i++, obj.getaMaterno());
            ps.setString(i++, obj.getCalle());
            ps.setString(i++, obj.getNumeroCasa());
            ps.setString(i++, obj.getColonia());
            ps.setString(i++, obj.getCodigoPostal());
            ps.setString(i++, obj.getCelular());
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Cliente read(Integer id) {
        Cliente cliente = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Cliente.Q_BY_ID, id));
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                cliente = makeCliente(rs);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return cliente;
    }

    @Override
    public List<Cliente> read(String criterio) {
        List<Cliente> clientes = null;
        try {
            Conexion conexion = Conexion.getInstance();
            Statement st = conexion.getConnection().createStatement();
            ResultSet rs = st.executeQuery(String.format("%s %s", Cliente.Q_ALL, criterio));
            if (rs.next()){
                clientes.add(makeCliente(rs));
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clientes;
    }

    @Override
    public void update(Cliente obj) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(String.format("%s %s",Cliente.UPDATE, obj.getIdCliente()));
            Integer i = 1;
            ps.setInt(i++, obj.getIdCliente());
            ps.setString(i++, obj.getUsuario());
            ps.setString(i++, obj.getContrasena());
            ps.setString(i++, obj.getNombre());
            ps.setString(i++, obj.getaPaterno());
            ps.setString(i++, obj.getaMaterno());
            ps.setString(i++, obj.getCalle());
            ps.setString(i++, obj.getNumeroCasa());
            ps.setString(i++, obj.getCodigoPostal());
            ps.setString(i++, obj.getCelular());
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Cliente.DELETE);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private Cliente makeCliente(ResultSet rs){
        Integer i = 1;
        Cliente cliente = null;
        try {
            cliente = new Cliente(
                    rs.getInt(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++),
                    rs.getString(i++)
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cliente;
    }

    @Override
    public Integer getLastIndex() {
        Integer index = null;
        try {
            Conexion conexion = Conexion.getInstance();
            PreparedStatement ps = conexion.getConnection().prepareStatement(Cliente.LAST_INDEX);
            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                index = rs.getInt(1);
            }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return index + 1;
    }
}
