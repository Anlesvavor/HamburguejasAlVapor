
package servlets;

import conexiones.Conexion;
import implementaciones.InventarioDAO;
import modelos.Inventario;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LeJesusjar
 */
@WebServlet(name = "InventarioSVT", urlPatterns = {"/InventarioSVT"})
public class InventarioSVT extends HttpServlet {
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        new InventarioDAO().create(new Inventario(
                Integer.parseInt(request.getParameter("intIdPlatillo")),
                request.getParameter("txtProducto"),
                request.getParameter("txtDescripcion"),
                Integer.parseInt(request.getParameter("intCantidad"))
        ));

        response.sendRedirect("administracion.jsp");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
