
package servlets;

import conexiones.Conexion;
import implementaciones.PlatilloDAO;
import modelos.Platillo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LeJesusjar
 */
@WebServlet(name = "PlatilloSVT", urlPatterns = {"/PlatilloSVT"})
public class PlatilloSVT extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        new PlatilloDAO().create(new Platillo(
                Integer.parseInt(request.getParameter("IntIdPlatillo")),
                request.getParameter("txtTipo"),
                request.getParameter("txtNomPlato"),
                request.getParameter("txtDescripcion"),
                Float.parseFloat(request.getParameter("IntPrecio"))
        ));

        response.sendRedirect("administracion.jsp");

    }
    

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
