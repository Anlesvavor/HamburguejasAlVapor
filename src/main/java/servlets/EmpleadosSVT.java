/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlets;

import conexiones.Conexion;
import implementaciones.EmpleadoDAO;
import modelos.Empleado;

import java.io.IOException;
//import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LeJesusjar
 */
@WebServlet(name = "EmpleadosSVT", urlPatterns = {"/EmpleadosSVT"})
public class EmpleadosSVT extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        new EmpleadoDAO().create(new Empleado(
                Integer.parseInt(request.getParameter("intidEmnpleado")),
                request.getParameter("txtNombre"),
                request.getParameter("txtTelefono"),
                request.getParameter("txtPuesto"),
                Integer.parseInt(request.getParameter("floatSueldoBas")),
                Float.parseFloat(request.getParameter("floatBonus")),
                Integer.parseInt(request.getParameter("intPersimo"))
        ));
    }
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
