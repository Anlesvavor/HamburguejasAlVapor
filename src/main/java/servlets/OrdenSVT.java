package servlets;

import conexiones.Conexion;
import implementaciones.ClienteDAO;
import implementaciones.OrdenDAO;
import implementaciones.PlatilloDAO;
import modelos.Orden;
import modelos.Platillo;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LeJesusjar
 */
@WebServlet(name = "OrdenSVT", urlPatterns = {"/OrdenSVT"})
public class OrdenSVT extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrdenSVT</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrdenSVT at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        //JOptionPane.showMessageDialog(null, txtPlatillo);

        new OrdenDAO().create(new Orden(
                Integer.parseInt(request.getParameter("intIdOrden")),
                new ClienteDAO().read(Integer.parseInt(request.getParameter("intIdCliente"))),
                new PlatilloDAO().read(Integer.parseInt(request.getParameter("intIdPlatillo"))),
                Integer.parseInt(request.getParameter("intCantidad"))
        ));

        response.sendRedirect("primerpagina.jsp");
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
