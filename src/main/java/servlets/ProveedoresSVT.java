package servlets;

import conexiones.Conexion;
import implementaciones.ProveedorDAO;
import modelos.Proveedor;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LeJesusjar
 */
@WebServlet(name = "ProveedoresSVT", urlPatterns = {"/ProveedoresSVT"})

public class ProveedoresSVT extends HttpServlet {
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        new ProveedorDAO().create(new Proveedor(
                Integer.parseInt(request.getParameter("IntIdProveedor")),
                request.getParameter("txtNombre"),
                request.getParameter("txtProducto"),
                Integer.parseInt(request.getParameter("txtCantidad"))
        ));

        response.sendRedirect("administracion.jsp");

    }
    
    
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
