
package servlets;

import conexiones.Conexion;
import implementaciones.EmpleadoDAO;
import implementaciones.SucursalDAO;
import interfaces.IntEmpleadoDAO;
import interfaces.IntSucursalDAO;
import modelos.Sucursal;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author LeJesusjar
 */
@WebServlet(name = "SucursalSVT", urlPatterns = {"/SucursalSVT"})
public class SucursalSVT extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        new SucursalDAO().create(new Sucursal(
                Integer.parseInt(request.getParameter("txtIdSucursal")),
                request.getParameter("txtNombre"),
                request.getParameter("txtDireccion"),
                request.getParameter("txtTelefono"),
                new EmpleadoDAO().read(Integer.parseInt(request.getParameter("intGerente")))
        ));

        response.sendRedirect("administracion.jsp");

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
