package modelos;

import static modelos.Model.fieldsToInsert;

/**
 *
 * @author LeJesusjar
 */
public class Inventario {

    public static final String ID = "idProducto";
    public static final String FIELDS = "" +
            "idProducto," +
            " producto," +
            " descripcion," +
            " cantidad";
    public static final String TABLE = "inventario";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE %s = ", Q_ALL, ID);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES %s", TABLE, FIELDS, fieldsToInsert(4));
    public static final String UPDATE = String.format("UPDATE %s SET idProducto = ?, producto = ?, descripcion = ?," +
            " cantidad = ? WHERE %s =", TABLE, ID);
    public static final String DELETE = String.format("DELETE FROM %s WHERE %s = ?", TABLE, ID);

    public static final String LAST_INDEX = String.format("SELECT MAX(%s) FROM %s", ID, TABLE);

    private Integer idProducto;
    private String producto;
    private String descripcion;
    private Integer cantidad;

    public Inventario() {
    }

    public Inventario(Integer idProducto, String producto, String descripcion, Integer cantidad) {
        this.idProducto = idProducto;
        this.producto = producto;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
