
package modelos;

import static modelos.Model.fieldsToInsert;

/**
 *
 * @author LeJesusjar
 */
public class Empleado {

    public static final String ID = "idEmpleado";
    public static final String FIELDS = "" +
            "idEmpleado," +
            " nombre," +
            " telefono," +
            " puesto," +
            " sueldoBase," +
            " bonos," +
            " permiso";
    public static final String TABLE = "empleado";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE %s = ", Q_ALL, ID);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES %s", TABLE, FIELDS, fieldsToInsert(8));
    public static final String UPDATE = String.format("UPDATE %s SET idEmpleado = ?, nombre = ?, telefono = ?," +
            " puesto = ?, sueldoBase = ?, bonos = ?, permiso = ? WHERE %s =", TABLE, ID);
    public static final String DELETE = String.format("DELETE FROM %s WHERE %s = ?", TABLE, ID);

    public static final String LAST_INDEX = String.format("SELECT MAX(%s) FROM %s", ID, TABLE);


    private Integer idEmpleado;
    private String nombre;
    private String telefono;
    private String puesto;
    private Integer sueldoBase;
    private Float bonos;
    private Integer permiso;

    public Empleado() {
    }

    public Empleado(Integer idEmpleado, String nombre, String telefono, String puesto, Integer sueldoBase, Float bonos, Integer permiso) {
        this.idEmpleado = idEmpleado;
        this.nombre = nombre;
        this.telefono = telefono;
        this.puesto = puesto;
        this.sueldoBase = sueldoBase;
        this.bonos = bonos;
        this.permiso = permiso;
    }

    public Integer getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(Integer idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public Integer getSueldoBase() {
        return sueldoBase;
    }

    public void setSueldoBase(Integer sueldoBase) {
        this.sueldoBase = sueldoBase;
    }

    public Float getBonos() {
        return bonos;
    }

    public void setBonos(Float bonos) {
        this.bonos = bonos;
    }

    public Integer getPermiso() {
        return permiso;
    }

    public void setPermiso(Integer permiso) {
        this.permiso = permiso;
    }

}
