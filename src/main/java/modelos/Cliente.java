
package modelos;

import java.util.ArrayList;
import java.util.List;

import static modelos.Model.fieldsToInsert;

/**
 *
 * @author LeJesusjar
 */
public class Cliente {

    public static final String ID = "idCliente";
    public static final String FIELDS = "" +
            "idCliente," +
            " usuario," +
            " contrasena," +
            " nombre," +
            " aPaterno," +
            " aMaterno," +
            " calle," +
            " numeroCasa," +
            " colonia," +
            " codigoPostal," +
            " celular";
    public static final String TABLE = "cliente";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE %s = ", Q_ALL, ID);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES (%s)", TABLE, FIELDS, fieldsToInsert(11));
    public static final String UPDATE = String.format("UPDATE %s SET idCliente = ?, usuario = ?, contrasena = ?," +
            " nombre = ?, aPaterno = ?, aMaterno = ?, calle = ?, numeroCasa = ?, colonia = ?, codigoPostal= ?, celular = ? WHERE %s =", TABLE, ID);
    public static final String DELETE = String.format("DELETE FROM %s WHERE %s = ?", TABLE, ID);

    public static final String LAST_INDEX = String.format("SELECT MAX(%s) FROM %s", ID, TABLE);

    private Integer idCliente;
    private String usuario;
    private String contrasena;
    private String nombre;
    private String aPaterno;
    private String aMaterno;
    private String calle;
    private String numeroCasa;
    private String colonia;
    private String codigoPostal;
    private String celular;

    public Cliente() {
    }

    public Cliente(Integer idCliente, String usuario, String contrasena, String nombre, String aPaterno, String aMaterno, String calle, String numeroCasa, String colonia, String codigoPostal, String celular) {
        this.idCliente = idCliente;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.nombre = nombre;
        this.aPaterno = aPaterno;
        this.aMaterno = aMaterno;
        this.calle = calle;
        this.numeroCasa = numeroCasa;
        this.colonia = colonia;
        this.codigoPostal = codigoPostal;
        this.celular = celular;
    }

    public Integer getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Integer idCliente) {
        this.idCliente = idCliente;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getaPaterno() {
        return aPaterno;
    }

    public void setaPaterno(String aPaterno) {
        this.aPaterno = aPaterno;
    }

    public String getaMaterno() {
        return aMaterno;
    }

    public void setaMaterno(String aMaterno) {
        this.aMaterno = aMaterno;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(String numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    public String getColonia() {
        return colonia;
    }

    public void setColonia(String colonia) {
        this.colonia = colonia;
    }

    public String getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(String codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }
}
