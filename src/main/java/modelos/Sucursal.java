
package modelos;

import static modelos.Model.fieldsToInsert;

/**
 *
 * @author LeJesusjar
 */
public class Sucursal {

    public static final String ID = "idSucursal";
    public static final String FIELDS = "" +
            "idSucursal," +
            " nombre," +
            " direccion," +
            " telefono," +
            " idGerente";
    public static final String TABLE = "sucursal";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE %s = ", Q_ALL, ID);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES %s", TABLE, FIELDS, fieldsToInsert(4));
    public static final String UPDATE = String.format("UPDATE %s SET idSucursal = ?, nombre = ?, direccion = ?, telefono = ?, idGerente = ? WHERE %s =", TABLE, ID);
    public static final String DELETE = String.format("DELETE FROM %s WHERE %s = ?", TABLE, ID);


    public static final String LAST_INDEX = String.format("SELECT MAX(%s) FROM %s", ID, TABLE);


    private Integer idOrden;
    private Cliente cliente;
    private Platillo platillo;
    private Integer cantidad;


    private Integer idSucursal;
    private String nombre;
    private String direccion;
    private String telefono;
    private Empleado gerente;

    public Sucursal() {
    }

    public Sucursal(Integer idSucursal, String nombre, String direccion, String telefono, Empleado gerente) {
        this.idSucursal = idSucursal;
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
        this.gerente = gerente;
    }

    public Integer getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Integer idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Empleado getGenrente() {
        return gerente;
    }

    public void setGenrente(Empleado gerente) {
        this.gerente = gerente;
    }
}
