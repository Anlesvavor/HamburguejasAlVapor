
package modelos;

import static modelos.Model.fieldsToInsert;

/**
 *
 * @author LeJesusjar
 */
public class Orden {

    public static final String ID = "idOrden";
    public static final String FIELDS = "" +
            "idOrden," +
            " idCliente," +
            " idPlatillo," +
            " cantidad";
    public static final String TABLE = "orden";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE %s = ", Q_ALL, ID);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES %s", TABLE, FIELDS, fieldsToInsert(4));
    public static final String UPDATE = String.format("UPDATE %s SET idOrden = ?, idCliente = ?, idPlatillo = ?, cantidad = ? WHERE %s =", TABLE, ID);
    public static final String DELETE = String.format("DELETE FROM %s WHERE %s = ?", TABLE, ID);

    public static final String LAST_INDEX = String.format("SELECT MAX(%s) FROM %s", ID, TABLE);

    private Integer idOrden;
    private Cliente cliente;
    private Platillo platillo;
    private Integer cantidad;

    public Orden() {
    }

    public Orden(Integer idOrden, Cliente cliente, Platillo latillo, Integer cantidad) {
        this.idOrden = idOrden;
        this.cliente = cliente;
        this.platillo = platillo;
        this.cantidad = cantidad;
    }

    public Integer getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(Integer idOrden) {
        this.idOrden = idOrden;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Platillo getPlatillo() {
        return platillo;
    }

    public void setPlatillo(Platillo platillo) {
        this.platillo = platillo;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
