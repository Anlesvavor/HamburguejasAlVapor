
package modelos;

import static modelos.Model.fieldsToInsert;

/**
 *
 * @author LeJesusjar
 */
public class Proveedor {

    public static final String ID = "idProveedor";
    public static final String FIELDS = "" +
            "idProveedor," +
            " nombre," +
            " producto," +
            " cantidad";
    public static final String TABLE = "proveedor";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE %s = ", Q_ALL, ID);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES %s", TABLE, FIELDS, fieldsToInsert(4));
    public static final String UPDATE = String.format("UPDATE %s SET idProveedor = ?, nombre = ?, producto = ?," +
            " cantidad = ? WHERE %s =", TABLE, ID);
    public static final String DELETE = String.format("DELETE FROM %s WHERE %s = ?", TABLE, ID);

    public static final String LAST_INDEX = String.format("SELECT MAX(%s) FROM %s", ID, TABLE);


    private Integer idProveedor;
    private String nombre;
    private String producto;
    private Integer cantidad;

    public Proveedor() {
    }

    public Proveedor(Integer idProveedor, String nombre, String producto, Integer cantidad) {
        this.idProveedor = idProveedor;
        this.nombre = nombre;
        this.producto = producto;
        this.cantidad = cantidad;
    }

    public Integer getIdProveedor() {
        return idProveedor;
    }

    public void setIdProveedor(Integer idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }
}
