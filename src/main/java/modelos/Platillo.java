
package modelos;

import static modelos.Model.fieldsToInsert;

/**
 *
 * @author LeJesusjar
 */
public class Platillo {

    public static final String ID = "idPlatillo";
    public static final String FIELDS = "" +
            "idPlatillo," +
            " tipo," +
            " nomPlato," +
            " descripcion," +
            " precio";
    public static final String TABLE = "platillo";
    public static final String Q_ALL = String.format("SELECT %s FROM %s", FIELDS, TABLE);
    public static final String Q_BY_ID = String.format("%s WHERE %s = ", Q_ALL, ID);
    public static final String INSERT = String.format("INSERT INTO %s (%s) VALUES %s", TABLE, FIELDS, fieldsToInsert(8));
    public static final String UPDATE = String.format("UPDATE %s SET idPlatillo = ?, tipo = ?, nomPlato = ?," +
            " descripcion = ?, precio = ? WHERE %s =", TABLE, ID);
    public static final String DELETE = String.format("DELETE FROM %s WHERE %s = ?", TABLE, ID);

    public static final String LAST_INDEX = String.format("SELECT MAX(%s) FROM %s", ID, TABLE);

    private Integer idPlatillo;
    private String tipo;
    private String nomPlato;
    private String descripcion;
    private Float precio;

    public Platillo(Integer idPlatillo) {
        this.idPlatillo = idPlatillo;
    }

    public Platillo(Integer idPlatillo, String tipo, String nomPlato, String descripcion, Float precio) {
        this.idPlatillo = idPlatillo;
        this.tipo = tipo;
        this.nomPlato = nomPlato;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public Integer getIdPlatillo() {
        return idPlatillo;
    }

    public void setIdPlatillo(Integer idPlatillo) {
        this.idPlatillo = idPlatillo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNomPlato() {
        return nomPlato;
    }

    public void setNomPlato(String nomPlato) {
        this.nomPlato = nomPlato;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Float getPrecio() {
        return precio;
    }

    public void setPrecio(Float precio) {
        this.precio = precio;
    }
}

